#
# Copyright (C) 2022 Statix
# SPDX-License-Identifer: Apache-2.0
#

PRODUCT_SOONG_NAMESPACES += \
    device/oplus/camera

OPLUS_PATH := device/oplus/camera

PRODUCT_PACKAGES += \
    oplus-fwk \
    OplusCamera \
    OplusExSystemService \
    OplusAppPlatform \
    OplusGallery \

PRODUCT_BOOT_JARS += oplus-fwk

TARGET_USES_OPLUS_CAMERA := true

BOARD_ROOT_EXTRA_SYMLINKS += /odm:/data/user/0/com.oplus.camera/files/odm

# VNDK
PRODUCT_COPY_FILES += \
    prebuilts/vndk/v30/arm/arch-arm-armv7-a-neon/shared/vndk-core/libui.so:$(TARGET_COPY_OUT_VENDOR)/lib/libui-v30.so

OPLUS_CAMERA_PROPERTIES := \
    persist.vendor.camera.privapp.list=com.oplus.camera \
    persist.camera.privapp.list=com.oplus.camera \
    ro.oplus.system.camera.name=com.oplus.camera \
    ro.oplus.system.camera.flashlight=com.oplus.motor.flashlight \
    ro.oplus.camera.video_beauty.prefix=oplus.video.beauty. \
    ro.oplus.camera.video.beauty.switch=oplus.switch.video.beauty \
    ro.oplus.camera.speechassist=true \
    vendor.camera.skip_unconfigure.packagelist=com.oplus.camera \
    persist.camera.manufacturer=oplus \
    persist.camera.oem.package=com.oplus.camera \
    persist.sys.assert.panic=true \
    oplus.camera.packname=com.oplus.camera

PRODUCT_VENDOR_PROPERTIES += $(OPLUS_CAMERA_PROPERTIES)
PRODUCT_PRODUCT_PROPERTIES += $(OPLUS_CAMERA_PROPERTIES)

